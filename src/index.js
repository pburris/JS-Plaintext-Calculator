const Parser = require('./parser');
const evaluate = require('./evaluator');

const calculator = {
  evaluate: input => {
    const p = new Parser(input);
    const program = p.parse();
    return {
      output: p.errors.length ? [] : program.map(pg => evaluate(pg)),
      errors: p.errors,
    };
  },
};

module.exports = calculator;

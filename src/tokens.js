/**
 * Tokens
 */

exports.PLUS = 'PLUS';
exports.MINUS = 'MINUS';
exports.BANG = 'BANG';
exports.ASTERISK = 'ASTERISK';
exports.SLASH = 'SLASH';
exports.MODULO = 'MODULO';
exports.POWER = 'POWER';
exports.LPAREN = 'LPAREN';
exports.RPAREN = 'RPAREN';
exports.NUMBER = 'NUMBER';
exports.ILLEGAL = 'ILLEGAL';
exports.EOF = 'EOF';

// Token Types
const types = {
  PLUS: '+',
  MINUS: '-',
  BANG: '!',
  ASTERISK: '*',
  SLASH: '/',
  MODULO: '%',
  POWER: '^',
  LPAREN: '(',
  RPAREN: ')',
  NUMBER: '_',
  ILLEGAL: 'illegal',
  EOF: '',
};

/**
 * Create Token
 */
exports.newToken = type => ({
  type,
  literal: types[type],
});

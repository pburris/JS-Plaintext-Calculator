# JavaScript Calculator Interpreter

Text interpreter for simple math calculations.


## Example

Simple example:

```
import calculator from 'calculator';

let input = '5 * 12';
let { output } = calculator.evaluate(input);
console.log(output) // [60]
```
